using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using UnityEngine.Networking;

public class MinecraftPlugin : MonoBehaviour
{

    public delegate void DownloadProgressDelegate(float progress);
    public delegate void DownloadCompletedDelegate(bool status);
    public event DownloadProgressDelegate OnDownloadProgress = delegate { };
    public event DownloadCompletedDelegate OnDownloadCompleted = delegate { };

    private AndroidJavaObject minecraftPlugin;
    private string persistPath;
    private string addonsPath;

    private static bool isDestroying = false;
    private static MinecraftPlugin instance;
    public static MinecraftPlugin Instance
    {
        get
        {
            if (instance == null && !isDestroying)
            {
                GameObject go = new GameObject
                {
                    name = "MinecraftPE_plugin"
                };
                instance = go.AddComponent<MinecraftPlugin>();
            }
            return instance;
        }
    }

    private void OnDestroy()
    {
        isDestroying = true;
    }

    private void OnApplicationPause(bool paused)
    {
        string lastModName = PlayerPrefs.GetString("lastModName");

        if (!paused)
        {
            if (lastModName != "")
            {
                if (File.Exists(addonsPath + lastModName))
                    File.Delete(addonsPath + lastModName);
                PlayerPrefs.SetString("lastModName", "");
            }
        }
    }

    private void CheckAndCleanupLoadedFiles()
    {
        if (Directory.Exists(addonsPath))
        {
            var files = Directory.GetFiles(addonsPath, "*", SearchOption.TopDirectoryOnly);
            foreach (var file in files)
                File.Delete(file);
        }
    }

    private void Awake()
    {
        persistPath = Application.persistentDataPath;
        addonsPath = persistPath + "/addons/";

        CheckAndCleanupLoadedFiles();
#if UNITY_ANDROID && !UNITY_EDITOR
            using (var javaUnityPlayer = new AndroidJavaClass("com.unity3d.player.UnityPlayer"))
            {
                using (var currentActivity = javaUnityPlayer.GetStatic<AndroidJavaObject>("currentActivity"))
                {
                    minecraftPlugin = new AndroidJavaObject("com.dv.minecraftplugin_main.MinecraftPlugin");
                    minecraftPlugin.CallStatic("Initialize", currentActivity);
                    Debug.Log("PLUGIN INIT");
                }
            }
#endif
    }

    private void CallAddModNative(string modName)
    {
#if !UNITY_EDITOR && UNITY_ANDROID
        minecraftPlugin.CallStatic("AddMod", addonsPath + modName);
#endif
    }

    public void AddModFromApp(string modName)
    {
        if (!File.Exists(addonsPath + modName))
            MoveModToPersistFolder(modName);
        CallAddModNative(modName);
    }

    public void AddModFromNetwork(string url, string modName)
    {
        CheckDirectory();
        if (!File.Exists(addonsPath + modName))
        {
            DownloadMod(url, modName, (bool status) =>
            {
                OnDownloadCompleted(status);
                if (status)
                {
                    PlayerPrefs.SetString("lastModName", modName);
                    CallAddModNative(modName);
                }
            });
        }
        else
            CallAddModNative(modName);
    }

    private void DownloadMod(string url, string modName, Action<bool> onComplete) =>
        StartCoroutine(FileDownloader(url, addonsPath + modName, onComplete));

    private void CheckDirectory()
    {
        if (!Directory.Exists(addonsPath))
            Directory.CreateDirectory(addonsPath);
    }

    private void MoveModToPersistFolder(string modName)
    {
        string inputPath = Application.streamingAssetsPath + "/addons/" + modName;
        string outputPath = addonsPath + modName;

        using (UnityWebRequest webRequest = UnityWebRequest.Get(inputPath))
        {
            webRequest.SendWebRequest();
            while (!webRequest.isDone) { }
            byte[] loadedBytes = webRequest.downloadHandler.data;
            CheckDirectory();
            File.WriteAllBytes(outputPath, loadedBytes);
        }
    }

    private IEnumerator FileDownloader(string url, string path, Action<bool> onComplete)
    {
        using (UnityWebRequest webRequest = new UnityWebRequest(url))
        {
            var downloadHandler = new DownloadHandlerBuffer();
            webRequest.downloadHandler = downloadHandler;

            UnityWebRequestAsyncOperation webRequestOperation = webRequest.SendWebRequest();
            while (!webRequestOperation.isDone)
            {
#if UNITY_EDITOR
                Debug.Log("Progress: " + webRequestOperation.progress);
#endif
                OnDownloadProgress?.Invoke(webRequestOperation.progress);
                yield return new WaitForSeconds(0.1f);
            }
            if (webRequest.result == UnityWebRequest.Result.ConnectionError
                || webRequest.result == UnityWebRequest.Result.ProtocolError)
            {
                Debug.Log(webRequest.error);
                onComplete?.Invoke(false);
            }
            else
            {
#if UNITY_EDITOR
                Debug.Log("Downloaded to: " + path);
#endif
                byte[] loadBytes = webRequest.downloadHandler.data;
                CheckDirectory();
                File.WriteAllBytes(path, loadBytes);
                onComplete?.Invoke(true);
            }
        }
    }
}