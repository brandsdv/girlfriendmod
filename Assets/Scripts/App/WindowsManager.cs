using System.Collections.Generic;
using UnityEngine;

public class WindowsManager : MonoBehaviour
{

    public enum WindowType
    {
        Main,
        Help,
        Policy,
        Disclaimer
    }

    private Stack<WindowType> windowsStack;

    [SerializeField]
    private CanvasGroup mainScreen;
    [SerializeField]
    private CanvasGroup helpScreen;
    [SerializeField]
    private CanvasGroup policyScreen;
    [SerializeField]
    private CanvasGroup disclaimerScreen;
    [SerializeField]
    private CanvasGroup loadingPopup;

    private static WindowsManager instance;
    public static WindowsManager Instance => instance;

    private void Awake()
    {
        instance = this;
        windowsStack = new Stack<WindowType>();
        windowsStack.Push(WindowType.Main);
    }

    private void ResetBlockRaycasts()
    {
        mainScreen.blocksRaycasts = false;
        helpScreen.blocksRaycasts = false;
        policyScreen.blocksRaycasts = false;
        disclaimerScreen.blocksRaycasts = false;
    }

    private void HideAllScreens()
    {
        mainScreen.alpha = 0;
        helpScreen.alpha = 0;
        policyScreen.alpha = 0;
        disclaimerScreen.alpha = 0;
        loadingPopup.alpha = 0;
    }

    public void ShowLoadingAdPopup()
    {
        Debug.LogWarning("SHOW POPUP");
        //       ResetBlockRaycasts();
        loadingPopup.blocksRaycasts = true;
        loadingPopup.alpha = 1;
    }

    public void HideLoadingAdPopup()
    {
        Debug.LogWarning("HIDE POPUP");
 //       ResetBlockRaycasts();
        loadingPopup.blocksRaycasts = false;
        loadingPopup.alpha = 0;
    }

    public void ShowMainScreen()
    {
        windowsStack.Push(WindowType.Main);
        AppCore.Instance.UpdateHeaderPanel(WindowType.Main);
        ResetBlockRaycasts();
        HideAllScreens();
        mainScreen.blocksRaycasts = true;
        mainScreen.alpha = 1;
    }

    public void ShowHelpScreen()
    {
        windowsStack.Push(WindowType.Help);
        AppCore.Instance.UpdateHeaderPanel(WindowType.Help);
        ResetBlockRaycasts();
        HideAllScreens();
        helpScreen.blocksRaycasts = true;
        helpScreen.alpha = 1;
    }

    private void HideHelpScreen()
    {
        AdsManager.Instance.ShowInterstitial();
        ResetBlockRaycasts();
        helpScreen.alpha = 0;
    }

    public void ShowPolicyScreen()
    {
        windowsStack.Push(WindowType.Policy);
        AppCore.Instance.UpdateHeaderPanel(WindowType.Policy);
        ResetBlockRaycasts();
        HideAllScreens();
        policyScreen.blocksRaycasts = true;
        policyScreen.alpha = 1;
    }

    private void HidePolicyScreen()
    {
        AdsManager.Instance.ShowInterstitial();
        ResetBlockRaycasts();
        policyScreen.alpha = 0;
    }

    public void ShowDisclaimerScreen()
    {
        windowsStack.Push(WindowType.Disclaimer);
        AppCore.Instance.UpdateHeaderPanel(WindowType.Disclaimer);
        ResetBlockRaycasts();
        HideAllScreens();
        disclaimerScreen.blocksRaycasts = true;
        disclaimerScreen.alpha = 1;
    }

    private void HideDisclaimerScreen()
    {
        AdsManager.Instance.ShowInterstitial();
        ResetBlockRaycasts();
        disclaimerScreen.alpha = 0;
    }

    public void HideLastWindow()
    {
        if (windowsStack.Count > 1)
        {
            var lastWindow = windowsStack.Pop();
            switch (lastWindow)
            {
                case WindowType.Help: HideHelpScreen(); break;
                case WindowType.Policy: HidePolicyScreen(); break;
                case WindowType.Disclaimer: HideDisclaimerScreen(); break;
            }

            var nextWindow = windowsStack.Pop();
            switch (nextWindow)
            {
                case WindowType.Main: ShowMainScreen(); break;
                case WindowType.Help: ShowHelpScreen(); break;
                case WindowType.Policy: ShowPolicyScreen(); break;
                case WindowType.Disclaimer: ShowDisclaimerScreen(); break;
            }
        }
    }


}
