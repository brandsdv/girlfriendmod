using TMPro;
using UnityEngine;
using static WindowsManager;

public class HeaderPanel : MonoBehaviour
{
    [SerializeField]
    private GameObject backButton;
    [SerializeField]
    private GameObject helpButton;
    [SerializeField]
    private TextMeshProUGUI headerText;

    public void OnBackButtonClick() => WindowsManager.Instance.HideLastWindow();

    public void OnHelpButtonClick() => WindowsManager.Instance.ShowHelpScreen();

    private void ResetPanelElements()
    {
        backButton.SetActive(false);
        helpButton.SetActive(false);
    }

    public void UpdatePanel(WindowType windowType)
    {
        ResetPanelElements();
        switch(windowType)
        {
            case WindowType.Main:
            {
                headerText.text = Lean.Localization.LeanLocalization.GetTranslationText("title");
                helpButton.SetActive(true);
                break;
            }
            case WindowType.Help:
            {
                headerText.text = Lean.Localization.LeanLocalization.GetTranslationText("help_header");
                backButton.SetActive(true);
                break;
            }
            case WindowType.Policy:
            {
                headerText.text = Lean.Localization.LeanLocalization.GetTranslationText("policy_header");
                backButton.SetActive(true);
                break;
            }
            case WindowType.Disclaimer:
            {
                headerText.text = "DISCLAIMER";
                backButton.SetActive(true);
                break;
            }
        }
    }

    public void UpdateHeaderText(string text) => headerText.text = text;
}
