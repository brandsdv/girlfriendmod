﻿using UnityEngine;
using UnityEngine.UI;
using GoogleMobileAds.Api;
using static WindowsManager;
using System.Collections;
using UnityEngine.Advertisements;

public class AppCore : MonoBehaviour
{

    [SerializeField]
    private HeaderPanel headerPanel;
    [SerializeField]
    private Button installButtonRu;
    [SerializeField]
    private Button installButtonEn;
    [SerializeField]
    private GameObject mainScrollViewRu;
    [SerializeField]
    private GameObject mainScrollViewEn;
    [SerializeField]
    private string addonName = "girlfriend2.mcaddon";
    [SerializeField]
    private Button youtubeButtonRu;
    [SerializeField]
    private Button youtubeButtonEn;

    public static AppCore Instance { get; private set; }

    private void Awake()
    {
        Instance = this;
        // TEST CODE, remove later
        mainScrollViewRu.SetActive(false);
        mainScrollViewEn.SetActive(false);
    }

    private void InternetConnectionHandlerRewarded(bool result)
    {
        if (result)
        {
            Debug.Log("TRUE");
            AdsManager.Instance.ShowRewardedAdsCombined(() =>
            {
                MinecraftPlugin.Instance.AddModFromApp(addonName);
            });
        }
        else
        {
            Debug.Log("FALSE");
            MinecraftPlugin.Instance.AddModFromApp(addonName);
        }
    }

    private void InternetConnectionHandler(bool result)
    {
        Debug.Log("RESULT: " + result);
        if (result)
        {
            AdsManager.Instance.LoadAdmobInterstitial();
        }
        else
            WindowsManager.Instance.HideLoadingAdPopup();
    }

    private void Start()
    {
        switch (Lean.Localization.LeanLocalization.CurrentLanguage)
        {
            case "Russian":
            {
                mainScrollViewRu.SetActive(true);
                mainScrollViewEn.SetActive(false);
                installButtonRu.onClick.AddListener(() =>
                {
                    InternetHelper.Instance.CheckInternetConnection(InternetConnectionHandlerRewarded);
                });
                youtubeButtonRu.onClick.AddListener(() =>
                {
                    Application.OpenURL("https://youtu.be/Y-e8J95UlX0");
                });
                break;
            }
            case "English":
            {
                mainScrollViewRu.SetActive(false);
                mainScrollViewEn.SetActive(true);
                installButtonEn.onClick.AddListener(() =>
                {
                    InternetHelper.Instance.CheckInternetConnection(InternetConnectionHandlerRewarded);
                });
                youtubeButtonEn.onClick.AddListener(() =>
                {
                    Application.OpenURL("https://youtu.be/Y-e8J95UlX0");
                });
                break;
            }
        }

        InternetHelper.Instance.CheckInternetConnection(InternetConnectionHandler);
    }

    public void UpdateHeaderPanel(WindowType windowType)
    {
        headerPanel.UpdatePanel(windowType);
    }
}