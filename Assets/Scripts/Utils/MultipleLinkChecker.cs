using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.EventSystems;

public class MultipleLinkChecker : MonoBehaviour, IPointerClickHandler
{
    [Serializable]
    public class LinkDescription
    {
        public enum Type
        {
            Url,
            Email
        }
        public string linkId;
        public Type type;
        public string targetUrl;
    }

    [SerializeField]
    private List<LinkDescription> links;

    private TextMeshProUGUI pTextMeshPro;
    private Camera pCamera;

    protected virtual void Awake()
    {
        pTextMeshPro = GetComponent<TextMeshProUGUI>();
        pCamera = Camera.main;
    }

    public void OnPointerClick(PointerEventData eventData)
    {
        Debug.Log("Pointer click!");
        int linkIndex = TMP_TextUtilities.FindIntersectingLink(pTextMeshPro, Input.mousePosition, pCamera);
        if (linkIndex != -1)
        { // was a link clicked?
            TMP_LinkInfo linkInfo = pTextMeshPro.textInfo.linkInfo[linkIndex];
            string linkId = linkInfo.GetLinkID();
            var result = links.Find((ld) => ld.linkId == linkId);
            if (result != null)
            {
                if(result.type==LinkDescription.Type.Email)
                    Application.OpenURL("mailto:" + result.targetUrl + "?subject:" + "" + "&body:" + "");
                else
                    Application.OpenURL(result.targetUrl);
            }
        }
    }
}
