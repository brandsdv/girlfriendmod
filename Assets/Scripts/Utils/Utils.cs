using UnityEngine;

public static class Utils
{
    public enum DeviceType
    {
        Phone,
        Tablet,
        Unknown
    }

    private static float DeviceDiagonalSizeInInches()
    {
        float screenWidth = Screen.width / Screen.dpi;
        float screenHeight = Screen.height / Screen.dpi;
        float diagonalInches = Mathf.Sqrt(Mathf.Pow(screenWidth, 2) + Mathf.Pow(screenHeight, 2));

        return diagonalInches;
    }

    public static DeviceType GetDeviceType()
    {
#if UNITY_EDITOR
        return DeviceType.Phone;
#elif UNITY_IOS
        bool deviceIsIpad = UnityEngine.iOS.Device.generation.ToString().Contains("iPad");
        if (deviceIsIpad)
            return DeviceType.Tablet;
        bool deviceIsIphone = UnityEngine.iOS.Device.generation.ToString().Contains("iPhone");
        if (deviceIsIphone)
            return DeviceType.Phone;
#endif

#pragma warning disable CS0162 // ��������� ������������ ���
        float aspectRatio = (float)Mathf.Max(Screen.width, Screen.height) / Mathf.Min(Screen.width, Screen.height);
#pragma warning restore CS0162 // ��������� ������������ ���
        bool isTablet = (DeviceDiagonalSizeInInches() > 6.5f && aspectRatio < 2f);
        if (isTablet)
            return DeviceType.Tablet;
        else
            return DeviceType.Phone;
    }
}
