﻿using System.Linq;

public static class Extensions
{
    public static string FirstCharToUpper(this string input)
    {
        if(!string.IsNullOrEmpty(input))
            return input.First().ToString().ToUpper() + input.Substring(1);
        return "";
    }

    public static string FirstCharToLower(this string input)
    {
        if (!string.IsNullOrEmpty(input))
            return input.First().ToString().ToLower() + input.Substring(1);
        return "";
    }
}