﻿using UnityEngine;

public class MobileRuntimeDestroyer : MonoBehaviour
{

    void Start()
    {
        if (!Application.isEditor && Application.isPlaying)
            Destroy(gameObject);
    }

}
