using System;
using System.Collections;
using UnityEngine;
using UnityEngine.Networking;

public class InternetHelper : MonoBehaviour
{
    private static InternetHelper instance = null;

    public static InternetHelper Instance
    {
        get
        {
            Init();
            return instance;
        }
        protected set => instance = value;
    }

    protected static void Init()
    {
        if (instance == null)
        {
            GameObject go = new GameObject("InternetHelper");
            instance = go.AddComponent<InternetHelper>();
        }
    }

    private IEnumerator CheckConnection(Action<bool> callback)
    {
        UnityWebRequest req = UnityWebRequest.Get("google.com");
        yield return req.SendWebRequest();
        if (req.isHttpError || req.isNetworkError)
            callback?.Invoke(false);
        else
            callback?.Invoke(true);
    }

    public void CheckInternetConnection(Action<bool> callback) =>
        StartCoroutine(CheckConnection(callback));
}