using TMPro;
using UnityEngine;
using UnityEngine.EventSystems;

public class LinkChecker : MonoBehaviour, IPointerClickHandler
{
    [SerializeField]
    private string targetUrl;

    private TextMeshProUGUI pTextMeshPro;
    private Camera pCamera;

    protected virtual void Awake()
    {
        pTextMeshPro = GetComponent<TextMeshProUGUI>();
        pCamera = Camera.main;
    }

    public void OnPointerClick(PointerEventData eventData)
    {
        Debug.Log("Pointer click!");
        int linkIndex = TMP_TextUtilities.FindIntersectingLink(pTextMeshPro, Input.mousePosition, pCamera);
        if (linkIndex != -1)
        { // was a link clicked?
            TMP_LinkInfo linkInfo = pTextMeshPro.textInfo.linkInfo[linkIndex];
            if(linkInfo.GetLinkID() == "email")
                Application.OpenURL("mailto:" + targetUrl + "?subject:" + "" + "&body:" + "");
            else
                Application.OpenURL(targetUrl);
        }
    }


}
