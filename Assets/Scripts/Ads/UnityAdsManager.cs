﻿using System;
using System.Collections;
using UnityEngine;
using UnityEngine.Advertisements;


public class UnityAdsManager : MonoBehaviour, IUnityAdsListener
{
    public bool isTestMode = false;

    private const string androidAdsID = "4468234";
    private const string iOSAdsID = "4468235";

    private Action<ShowResult> showAdsCallback;
    public bool IsAdsReady { get; private set; }

    public static UnityAdsManager Instance { get; private set; }

    private void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
        }
        else
        {
            Debug.LogWarning("Multiple Ads Manager manager found, please use only one.", this);
        }
    }

    private void OnDestroy()
    {
        if (Instance == this)
        {
            Instance = null;
        }
    }

    private void Start()
    {
        string adsID = androidAdsID; // "";
        if (Application.platform == RuntimePlatform.Android)
            adsID = androidAdsID;
        else if (Application.platform == RuntimePlatform.IPhonePlayer)
            adsID = iOSAdsID;
        else if (Application.platform != RuntimePlatform.WindowsEditor) //TODO: anyone can add needed platforms to check
            Debug.LogError("ADS->: Current platform neither Android, nor iOS");
        Advertisement.Initialize(adsID, isTestMode);
        Advertisement.AddListener(this);
    }

    IEnumerator ShowBannerWhenReady()
    {
        while (!Advertisement.IsReady("Banner_Android")) // && !Advertisement.Banner.isLoaded)
        {
            Debug.Log("WAITING BANNER UNITY");
            yield return new WaitForSeconds(0.5f);
        }

        Advertisement.Banner.Show("Banner_Android");
    }

    public void ShowBanner()
    {
        Debug.Log("SHOW BANNER UNITY");
        Advertisement.Banner.SetPosition(BannerPosition.BOTTOM_CENTER);
        Advertisement.Banner.Load("Banner_Android");
        StartCoroutine(ShowBannerWhenReady());
    }
    
    public void ShowAd(string type, Action<ShowResult> callback = null)
    {
        showAdsCallback = callback;

        Debug.Log("IsVideoAdReady! Initialized -> " + Advertisement.isInitialized
                  + " Is Ready v -> " + Advertisement.IsReady() + " Is Ready v -> "
                  + Advertisement.IsReady("Interstitial_Android") + " Is Ready rev v -> "
                  + Advertisement.IsReady("Rewarded_Android"));

        if (Advertisement.IsReady(type))
        {
            Debug.Log("READY");
            Advertisement.Show(type);
        }
        else
        {
            Debug.Log("NOT READY");
        }
    }

    public bool IsVideoAdReady(string type)
    {
        Debug.Log("IsVideoAdReady! Initialized -> " + Advertisement.isInitialized
                  + " Is Ready v -> " + Advertisement.IsReady() + " Is Ready v -> " 
                  + Advertisement.IsReady("Interstitial_Android") + " Is Ready rev v -> "
                  + Advertisement.IsReady("Rewarded_Android"));
        return Advertisement.isInitialized && Advertisement.IsReady(type);
    }

    public void OnUnityAdsReady(string placementId)
    {
        IsAdsReady = true;
    }

    public void OnUnityAdsDidError(string message)
    {

    }

    public void OnUnityAdsDidStart(string placementId)
    {

    }

    public void OnUnityAdsDidFinish(string placementId, ShowResult showResult)
    {
        showAdsCallback(showResult);
        showAdsCallback = null;
    }
}
