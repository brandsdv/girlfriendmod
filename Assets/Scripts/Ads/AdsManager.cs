using GoogleMobileAds.Api;
using GoogleMobileAds.Common;
using System;
using System.Collections;
using UnityEngine;
using UnityEngine.Advertisements;

public class AdsManager : MonoBehaviour
{

    private const string ADMOB_BANNER_ID_ANDROID = "ca-app-pub-7041598119886359/4789757327";
    private const string ADMOB_INTERSTITITAL_ID_ANDROID = "ca-app-pub-7041598119886359/8631771741";
    private const string ADMOB_REWARDED_ID_ANDROID = "ca-app-pub-7041598119886359/6005608407";

    private const string ADMOB_BANNER_ID_TEST = "ca-app-pub-3940256099942544/2934735716";
    private const string ADMOB_INTERSTITITAL_ID_TEST = "ca-app-pub-3940256099942544/4411468910";
    private const string ADMOB_REWARDED_ID_TEST = "ca-app-pub-3940256099942544/1712485313";

    //private const string ADMOB_BANNER_ID_ANDROID = ADMOB_BANNER_ID_TEST;
    //private const string ADMOB_INTERSTITITAL_ID_ANDROID = ADMOB_INTERSTITITAL_ID_TEST;
    //private const string ADMOB_REWARDED_ID_ANDROID = ADMOB_REWARDED_ID_TEST;

    //private NativeAdsManager nativeAdsManager;

    [SerializeField]
    private float popupDelay = 5f;

    public bool IsAdmobReady { get; private set; } = false;

    private BannerView bannerView;
    private InterstitialAd interstitialAd;
    private RewardedAd rewardedAd;
    private bool rewardedAdFailedToLoad = false;
    private float waitTime = 0;
    private Coroutine loadingCoroutine;

    private Action<Reward> OnRewardEarned;
    private Action OnBannerLoaded;
    private Action OnBannerClosed;

    private static AdsManager instance;
    public static AdsManager Instance => instance;

    private void Awake()
    {
        if (instance == null)
            instance = this;
        else
            Destroy(gameObject);
        //        nativeAdsManager = GetComponent<NativeAdsManager>();
        DontDestroyOnLoad(gameObject);
        InitAds(() => { }, () => { });
    }

    public void InitAds(Action onBannerLoaded, Action onBannerClosed)
    {
        OnBannerLoaded = onBannerLoaded;
        OnBannerClosed = onBannerClosed;
        MobileAds.Initialize(HandleInitCompleteAction);
    }

    private void HandleInitCompleteAction(InitializationStatus initstatus)
    {
        MobileAdsEventExecutor.ExecuteInUpdate(() =>
        {
            //RequestBanner();
            CreateAndLoadInterstitialAd();
            CreateAndLoadRewardedAd();
            IsAdmobReady = true;
            Debug.Log("IS ADMOB READY: " + IsAdmobReady);
            //RequestNewNative();
        });
    }

    private AdRequest CreateAdRequest() => new AdRequest.Builder().Build();

    public void RequestBanner()
    {
#if UNITY_ANDROID// || UNITY_EDITOR
        string adUnitId = ADMOB_BANNER_ID_ANDROID;
#elif UNITY_IPHONE
        string adUnitId = ADMOB_BANNER_ID_IOS;
#else
        string adUnitId = "unexpected_platform";
#endif
        if (bannerView != null)
        {
            bannerView.OnAdLoaded -= HandleOnBannerLoaded;
            bannerView.OnAdClosed -= HandleOnBannerClosed;
            bannerView.Destroy();
        }

        AdSize adaptiveSize =
        AdSize.GetCurrentOrientationAnchoredAdaptiveBannerAdSizeWithWidth(AdSize.FullWidth);
        bannerView = new BannerView(adUnitId, adaptiveSize, AdPosition.Bottom);
        bannerView.OnAdLoaded += HandleOnBannerLoaded;
        bannerView.OnAdClosed += HandleOnBannerClosed;
        bannerView.OnAdFailedToLoad += HandleOnBannerFailed;
        bannerView.LoadAd(CreateAdRequest());

        Debug.Log("REQUEST BANNER ADMOB");
    }

    private void HandleOnBannerFailed(object sender, EventArgs args)
    {
        //Debug.Log("ADMOB: banner failed to load");
        UnityAdsManager.Instance.ShowBanner();
    }

    private void HandleOnBannerLoaded(object sender, EventArgs args)
    {
        OnBannerLoaded?.Invoke();
    }

    private void HandleOnBannerClosed(object sender, EventArgs args)
    {
        OnBannerClosed?.Invoke();
    }

    private void CreateAndLoadInterstitialAd()
    {
#if UNITY_ANDROID //|| UNITY_EDITOR
        string adUnitId = ADMOB_INTERSTITITAL_ID_ANDROID;
#elif UNITY_IPHONE
        string adUnitId = ADMOB_INTERSTITITAL_ID_IOS;
#else
        string adUnitId = "unexpected_platform";
#endif
        if (interstitialAd != null)
        {
            interstitialAd.OnAdClosed -= HandleOnInterstitialAdClosed;
            interstitialAd.Destroy();
        }
        interstitialAd = new InterstitialAd(adUnitId);
        interstitialAd.OnAdClosed += HandleOnInterstitialAdClosed;
        interstitialAd.LoadAd(CreateAdRequest());
    }

    public bool IsInterstitialLoaded() => interstitialAd != null && interstitialAd.IsLoaded();

    public bool IsRewardedAdLoaded() => rewardedAd != null && rewardedAd.IsLoaded() && !rewardedAdFailedToLoad;

    public void CreateAndLoadInterstitial()
    {
        MobileAdsEventExecutor.ExecuteInUpdate(() => CreateAndLoadInterstitialAd());
    }

    public void ShowInterstitial()
    {
        if (IsInterstitialLoaded())
        {
            Debug.Log("SHOW");
            MobileAdsEventExecutor.ExecuteInUpdate(() => interstitialAd.Show());
        }
        else
        {
            Debug.Log("SHOW 2");
            CreateAndLoadInterstitial();
            if (UnityAdsManager.Instance.IsVideoAdReady("Interstitial_Android"))
                UnityAdsManager.Instance.ShowAd("Interstitial_Android", (ShowResult r) => { });
        }
    }

    public void HandleOnInterstitialAdClosed(object sender, EventArgs args)
    {
        CreateAndLoadInterstitialAd();
    }

    private void CreateAndLoadRewardedAd()
    {
#if UNITY_ANDROID //|| UNITY_EDITOR
        string adUnitId = ADMOB_REWARDED_ID_ANDROID;
#elif UNITY_IPHONE
        string adUnitId = ADMOB_REWARDED_ID_IOS;
#else
        string adUnitId = "unexpected_platform";
#endif
        rewardedAdFailedToLoad = false;
        rewardedAd = new RewardedAd(adUnitId);
        rewardedAd.OnUserEarnedReward += HandleUserEarnedReward;
        rewardedAd.OnAdClosed += HandleOnRewardedAdClosed;
        rewardedAd.OnAdFailedToLoad += HandleRewardedFailedToLoad;
        rewardedAd.LoadAd(CreateAdRequest());
    }

    private void HandleRewardedFailedToLoad(object sender, AdErrorEventArgs args)
    {
        rewardedAdFailedToLoad = true;
    }

    private void HandleUserEarnedReward(object sender, Reward args)
    {
        OnRewardEarned?.Invoke(args);
    }

    private void HandleOnRewardedAdClosed(object sender, EventArgs args)
    {
        MobileAdsEventExecutor.ExecuteInUpdate(CreateAndLoadRewardedAd);
    }

    private void ShowRewarded(Action<Reward> callbackEarned)
    {
        Debug.Log("ADS: SHOW REWARDED");
        if (rewardedAd.IsLoaded())
        {
            OnRewardEarned = callbackEarned;
            MobileAdsEventExecutor.ExecuteInUpdate(() => rewardedAd.Show());
        }
        else
        {
            Debug.LogWarning("Rewarded ad not loaded!");
        }
    }

    private IEnumerator LoadInterstitialsAdmob()
    {
        //       if (AdsManager.Instance.IsAdmobReady)
        //AdsManager.Instance.RequestBanner();
        //if (IsAdmobReady)
        //{
        MobileAdsEventExecutor.ExecuteInUpdate(() =>
        {
            RequestBanner();
        });
        //}
        //else
        //    UnityAdsManager.Instance.ShowBanner();

        waitTime = 0;
        CreateAndLoadInterstitial();
        WindowsManager.Instance.ShowLoadingAdPopup();
        bool admobLoaded = true;
        while (!IsInterstitialLoaded())
        {
            waitTime += Time.deltaTime;
            if (waitTime >= popupDelay)
            {
                admobLoaded = false;
                StopCoroutine(loadingCoroutine);
                loadingCoroutine = StartCoroutine(LoadInterstititalsUnity());
                //WindowsManager.Instance.HideLoadingAdPopup();
                yield break;
            }
            yield return null;
        }

        //if (admobLoaded)
        //{
        Debug.Log("SHOW INTERSTITITAL ADMOB");
        //RequestBanner();
        ShowAdmobInterstitial();
        //}
        //else
        //    UnityAdsManager.Instance.ShowBanner();
    }

    public void LoadAdmobInterstitial()
    {
        loadingCoroutine = StartCoroutine(LoadInterstitialsAdmob());
    }

    private IEnumerator LoadInterstititalsUnity()
    {
        waitTime = 0;
        //if(!IsAdmobReady)
        //UnityAdsManager.Instance.ShowBanner();
        //AdsManager.Instance.CreateAndLoadInterstitial();
        //WindowsManager.Instance.ShowLoadingAdPopup();
        bool unityAdsLoaded = true;
        while (!UnityAdsManager.Instance.IsVideoAdReady("Interstitial_Android"))
        {
            waitTime += Time.deltaTime;
            Debug.Log("Wait unity inters");
            if (waitTime >= popupDelay)
            {
                unityAdsLoaded = false;
                WindowsManager.Instance.HideLoadingAdPopup();
                waitTime = 0;
                yield break;
            }
            yield return null;
        }

        if (unityAdsLoaded)
        {
            Debug.Log("SHOW INTERSTITITAL UNITY");
            WindowsManager.Instance.HideLoadingAdPopup();
            UnityAdsManager.Instance.ShowAd("Interstitial_Android", (ShowResult result) => { });
        }
    }

    private void ShowAdmobInterstitial()
    {
        WindowsManager.Instance.HideLoadingAdPopup();
        ShowInterstitial();
    }

    public void ShowRewardedAdsCombined(Action onSuccess)
    {
        if (AdsManager.Instance.IsRewardedAdLoaded()) //&& false)
        {
            Debug.Log("ADS: show admob");
            AdsManager.Instance.ShowRewarded((Reward r) =>
            {
                onSuccess?.Invoke();
            });
        }
        else if (UnityAdsManager.Instance.IsVideoAdReady("Rewarded_Android")) // || true)
        {
            Debug.Log("ADS: show unity ads");
            UnityAdsManager.Instance.ShowAd("Rewarded_Android", (ShowResult rez) =>
            {
                if (rez == ShowResult.Finished)
                {
                    onSuccess?.Invoke();
                }
            });
        }
        else
        {
            Debug.Log("ADS: any ads not ready");
            onSuccess?.Invoke();
        }
    }

    //public void RequestNewNative() => nativeAdsManager.RequestNativeAd();

    //public void EnableDisableNative(bool isEnabled) => nativeAdsManager.EnableDisableNativeAd(isEnabled);
}


//using GoogleMobileAds.Api;
//using GoogleMobileAds.Common;
//using System;
//using System.Collections;
//using UnityEngine;
//using UnityEngine.Advertisements;

//public class AdsManager : MonoBehaviour
//{

//    private const string ADMOB_BANNER_ID_ANDROID = "ca-app-pub-7041598119886359/4789757327";
//    private const string ADMOB_INTERSTITITAL_ID_ANDROID = "ca-app-pub-7041598119886359/8631771741";
//    private const string ADMOB_REWARDED_ID_ANDROID = "ca-app-pub-7041598119886359/6005608407";

//    private const string ADMOB_BANNER_ID_TEST = "ca-app-pub-3940256099942544/2934735716";
//    private const string ADMOB_INTERSTITITAL_ID_TEST = "ca-app-pub-3940256099942544/4411468910";
//    private const string ADMOB_REWARDED_ID_TEST = "ca-app-pub-3940256099942544/1712485313";

//    //private const string ADMOB_BANNER_ID_ANDROID = ADMOB_BANNER_ID_TEST;
//    //private const string ADMOB_INTERSTITITAL_ID_ANDROID = ADMOB_INTERSTITITAL_ID_TEST;
//    //private const string ADMOB_REWARDED_ID_ANDROID = ADMOB_REWARDED_ID_TEST;

//    //private NativeAdsManager nativeAdsManager;

//    [SerializeField]
//    private float popupDelay = 5f;

//    public bool IsAdmobReady { get; private set; } = false;

//    private BannerView bannerView;
//    private InterstitialAd interstitialAd;
//    private RewardedAd rewardedAd;
//    private bool rewardedAdFailedToLoad = false;
//    private float waitTime = 0;
//    private Coroutine loadingCoroutine;

//    private Action<Reward> OnRewardEarned;
//    private Action OnBannerLoaded;
//    private Action OnBannerClosed;

//    private static AdsManager instance;
//    public static AdsManager Instance => instance;

//    private void Awake()
//    {
//        if (instance == null)
//            instance = this;
//        else
//            Destroy(gameObject);
//        //        nativeAdsManager = GetComponent<NativeAdsManager>();
//        DontDestroyOnLoad(gameObject);
//        InitAds(() => { }, () => { });
//    }

//    public void InitAds(Action onBannerLoaded, Action onBannerClosed)
//    {
//        OnBannerLoaded = onBannerLoaded;
//        OnBannerClosed = onBannerClosed;
//        MobileAds.Initialize(HandleInitCompleteAction);
//    }

//    private void HandleInitCompleteAction(InitializationStatus initstatus)
//    {
//        MobileAdsEventExecutor.ExecuteInUpdate(() =>
//        {
//            //RequestBanner();
//            //CreateAndLoadInterstitialAd();
//            CreateAndLoadRewardedAd();
//            IsAdmobReady = true;
//            Debug.Log("IS ADMOB READY: " + IsAdmobReady);
//            //RequestNewNative();
//        });
//    }

//    private AdRequest CreateAdRequest() => new AdRequest.Builder().Build();

//    public void RequestBanner()
//    {
//#if UNITY_ANDROID// || UNITY_EDITOR
//        string adUnitId = ADMOB_BANNER_ID_ANDROID;
//#elif UNITY_IPHONE
//        string adUnitId = ADMOB_BANNER_ID_IOS;
//#else
//        string adUnitId = "unexpected_platform";
//#endif
//        if (bannerView != null)
//        {
//            bannerView.OnAdLoaded -= HandleOnBannerLoaded;
//            bannerView.OnAdClosed -= HandleOnBannerClosed;
//            bannerView.Destroy();
//        }

//        AdSize adaptiveSize =
//        AdSize.GetCurrentOrientationAnchoredAdaptiveBannerAdSizeWithWidth(AdSize.FullWidth);
//        bannerView = new BannerView(adUnitId, adaptiveSize, AdPosition.Bottom);
//        bannerView.OnAdLoaded += HandleOnBannerLoaded;
//        bannerView.OnAdClosed += HandleOnBannerClosed;
//        bannerView.OnAdFailedToLoad += HandleOnBannerFailed;
//        bannerView.LoadAd(CreateAdRequest());

//        Debug.Log("REQUEST BANNER ADMOB");
//    }

//    private void HandleOnBannerFailed(object sender, EventArgs args)
//    {
//        Debug.Log("ADMOB: banner failed to load");
//        UnityAdsManager.Instance.ShowBanner();
//    }

//    private void HandleOnBannerLoaded(object sender, EventArgs args)
//    {
//        OnBannerLoaded?.Invoke();
//    }

//    private void HandleOnBannerClosed(object sender, EventArgs args)
//    {
//        OnBannerClosed?.Invoke();
//    }

//    private void CreateAndLoadInterstitialAd(Action callback = null)
//    {
//        //interstitialLoadedCb = callback;
//#if UNITY_ANDROID //|| UNITY_EDITOR
//        string adUnitId = ADMOB_INTERSTITITAL_ID_ANDROID;
//#elif UNITY_IPHONE
//        string adUnitId = ADMOB_INTERSTITITAL_ID_IOS;
//#else
//        string adUnitId = "unexpected_platform";
//#endif
//        if (interstitialAd != null)
//        {
//            interstitialAd.OnAdClosed -= HandleOnInterstitialAdClosed;
//            interstitialAd.Destroy();
//        }
//        interstitialAd = new InterstitialAd(adUnitId);
//        interstitialAd.OnAdClosed += HandleOnInterstitialAdClosed;
//        interstitialAd.OnAdFailedToLoad += HandleOnInterstitialAdFailed;
//        interstitialAd.OnAdLoaded += HandleOnInterstitialAdLoaded;
//        interstitialAd.LoadAd(CreateAdRequest());
//    }

//    //Action interstitialLoadedCb;

//    bool isFromStart = true;
//    public void HandleOnInterstitialAdLoaded(object sender, EventArgs args)
//    {
//        Debug.Log("ADMOB: inters loaded " + IsInterstitialLoaded() + " " + isFromStart);
//        //interstitialLoadedCb?.Invoke();
//        //CreateAndLoadInterstitialAd();

//        if (IsInterstitialLoaded() && isFromStart)
//        {
//            Debug.Log("SHOW NEW");
//            WindowsManager.Instance.HideLoadingAdPopup();
//            isFromStart = false;
//            MobileAdsEventExecutor.ExecuteInUpdate(() => interstitialAd.Show());
//        }
//    }

//    float currTime = 0;
//    IEnumerator StartWaitCoroutine()
//    {
//        while(true)
//        {
//            currTime += Time.deltaTime;
//            if(UnityAdsManager.Instance.IsVideoAdReady("Interstitial_Android"))
//            {
//                Debug.Log("->T3");
//                WindowsManager.Instance.HideLoadingAdPopup();
//                UnityAdsManager.Instance.ShowAd("Interstitial_Android", (ShowResult result) => { });
//                yield break;
//            }
//            else if(currTime>=5f)
//            {
//                Debug.Log("->T4");
//                WindowsManager.Instance.HideLoadingAdPopup();
//                yield break;
//            }
//            yield return null;
//        }
//    }

//    public void HandleOnInterstitialAdFailed(object sender, EventArgs args)
//    {
//        Debug.Log("ADMOB: inters failed to load");
//        //        WindowsManager.Instance.HideLoadingAdPopup();
//        if (isFromStart)
//        {
//            isFromStart = false;
//            if (UnityAdsManager.Instance.IsVideoAdReady("Interstitial_Android"))
//            {
//                Debug.Log("->T1");
//                WindowsManager.Instance.HideLoadingAdPopup();
//                UnityAdsManager.Instance.ShowAd("Interstitial_Android", (ShowResult result) => { });
//            }
//            else
//            {
//                Debug.Log("->T2");
//                StartCoroutine(StartWaitCoroutine());
//            }
//        }

//        //CreateAndLoadInterstitialAd();
//    }

//    public void HandleOnInterstitialAdClosed(object sender, EventArgs args)
//    {
//        CreateAndLoadInterstitialAd();
//    }


//    public bool IsInterstitialLoaded() => interstitialAd != null && interstitialAd.IsLoaded();

//    public bool IsRewardedAdLoaded() => rewardedAd != null && rewardedAd.IsLoaded() && !rewardedAdFailedToLoad;

//    public void CreateAndLoadInterstitial(/*Action callback*/)
//    {
//        MobileAdsEventExecutor.ExecuteInUpdate(() => CreateAndLoadInterstitialAd(/*callback*/));
//    }

//    public void ShowInterstitial()
//    {
//        if (IsInterstitialLoaded())
//        {
//            Debug.Log("SHOW");
//            MobileAdsEventExecutor.ExecuteInUpdate(() => interstitialAd.Show());
//        }
//        else
//        {
//            Debug.Log("SHOW 2");
//            //fromStart = false;
//            //WindowsManager.Instance.ShowLoadingAdPopup();
//            CreateAndLoadInterstitial(/*null*//*()=>ShowAdmobInterstitial()*/);
//            //if (UnityAdsManager.Instance.IsVideoAdReady("Interstitial_Android"))
//            //    UnityAdsManager.Instance.ShowAd("Interstitial_Android", (ShowResult r) => { });

//            // JUST FOR TEST
//            WindowsManager.Instance.ShowLoadingAdPopup();
//            if (UnityAdsManager.Instance.IsVideoAdReady("Interstitial_Android"))
//            {
//                Debug.Log("->T1");
//                WindowsManager.Instance.HideLoadingAdPopup();
//                UnityAdsManager.Instance.ShowAd("Interstitial_Android", (ShowResult result) => { });
//            }
//            else
//            {
//                Debug.Log("->T2");
//                StartCoroutine(StartWaitCoroutine());
//            }
//        }
//    }

//    //bool fromStart = true;

//    private void CreateAndLoadRewardedAd()
//    {
//#if UNITY_ANDROID //|| UNITY_EDITOR
//        string adUnitId = ADMOB_REWARDED_ID_ANDROID;
//#elif UNITY_IPHONE
//        string adUnitId = ADMOB_REWARDED_ID_IOS;
//#else
//        string adUnitId = "unexpected_platform";
//#endif
//        rewardedAdFailedToLoad = false;
//        rewardedAd = new RewardedAd(adUnitId);
//        rewardedAd.OnUserEarnedReward += HandleUserEarnedReward;
//        rewardedAd.OnAdClosed += HandleOnRewardedAdClosed;
//        rewardedAd.OnAdFailedToLoad += HandleRewardedFailedToLoad;
//        rewardedAd.LoadAd(CreateAdRequest());
//    }

//    private void HandleRewardedFailedToLoad(object sender, AdErrorEventArgs args)
//    {
//        rewardedAdFailedToLoad = true;
//    }

//    private void HandleUserEarnedReward(object sender, Reward args)
//    {
//        OnRewardEarned?.Invoke(args);
//    }

//    private void HandleOnRewardedAdClosed(object sender, EventArgs args)
//    {
//        MobileAdsEventExecutor.ExecuteInUpdate(CreateAndLoadRewardedAd);
//    }

//    public void ShowRewarded(Action<Reward> callbackEarned)
//    {
//        Debug.Log("ADS: SHOW REWARDED");
//        if (rewardedAd.IsLoaded())
//        {
//            OnRewardEarned = callbackEarned;
//            MobileAdsEventExecutor.ExecuteInUpdate(() => rewardedAd.Show());
//        }
//        else
//        {
//            Debug.LogWarning("Rewarded ad not loaded!");
//        }
//    }

//    public void LoadAdmobInterstitial()
//    {
//        MobileAdsEventExecutor.ExecuteInUpdate(() =>
//        {
//            RequestBanner();
//        });

//        CreateAndLoadInterstitial(
//        //    ()=>
//        //{
//        //    WindowsManager.Instance.HideLoadingAdPopup();
//        //    ShowAdmobInterstitial();
//        //}
//        );
//        WindowsManager.Instance.ShowLoadingAdPopup();
//    }

//    private void ShowAdmobInterstitial()
//    {
//        WindowsManager.Instance.HideLoadingAdPopup();
//        ShowInterstitial();
//    }

//    //public void RequestNewNative() => nativeAdsManager.RequestNativeAd();

//    //public void EnableDisableNative(bool isEnabled) => nativeAdsManager.EnableDisableNativeAd(isEnabled);
//}
