using GoogleMobileAds.Api;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class NativeAdsManager : MonoBehaviour
{

    private const string ADMOB_NATIVE_ID_IOS = "ca-app-pub-7041598119886359/5810511790";
    private const string ADMOB_NATIVE_ID_ANDROID = "ca-app-pub-7041598119886359/5836110682";
    private const string ADMOB_NATIVE_ID_TEST = "ca-app-pub-3940256099942544/2247696110";

    private UnifiedNativeAd nativeAd;
    private bool unifiedNativeAdLoaded;

    public bool nativeAdPanelReady { get; private set; } = false;

    // Below are the UI element that you need to assign from Unity Editor
    [SerializeField]
    private GameObject nativeAdPanel;
    //[SerializeField]
    //private GameObject adChoiceTexture;  //raw image
    [SerializeField]
    private GameObject appIcon;          //raw image
    [SerializeField]
    private GameObject headlines;        //text (mb change to textmesh?)
    [SerializeField]
    private GameObject starRating;       //group of raw images
    [SerializeField]
    private GameObject store;            //text (mb change to textmesh?)
    [SerializeField]
    private GameObject bodyText;         //text (mb change to textmesh?)
    [SerializeField]
    private GameObject bigImage;         //raw image   
    [SerializeField]
    private GameObject buttonText;       //text (mb change to textmesh?)
    [SerializeField]
    private GameObject buttonBack;
    [SerializeField]
    private Transform coloredStarsRoot;

    private AdLoader adLoader;

    private void ResetState()
    {
        if (!appIcon.activeSelf)
            appIcon.SetActive(true);
        if (!headlines.activeSelf)
            headlines.SetActive(true);
        if (!store.activeSelf)
            store.SetActive(true);
        if (!bodyText.activeSelf)
            bodyText.SetActive(true);
        if (!starRating.activeSelf)
            starRating.SetActive(true);
        if (!bigImage.activeSelf)
            bigImage.SetActive(true);
        if (!buttonText.activeSelf)
            buttonText.SetActive(true);
        if (!buttonBack.activeSelf)
            buttonBack.SetActive(true);
    }

    // Update is called once per frame
    private void Update()
    {
        if (unifiedNativeAdLoaded)
        {
            unifiedNativeAdLoaded = false;

            ResetState();

            Texture2D iconTexture = nativeAd.GetIconTexture();
            if (iconTexture != null)
            {
                appIcon.GetComponent<RawImage>().texture = iconTexture;
                if (!nativeAd.RegisterIconImageGameObject(appIcon))
                {
                    Debug.Log("RegisterIconImageGameObject Unsuccessfull");
                }
            }
            else
                appIcon.SetActive(false);

            string headline = nativeAd.GetHeadlineText();
            if (headline != null)
            {
                headlines.GetComponent<TextMeshProUGUI>().text = headline;
                if (!nativeAd.RegisterHeadlineTextGameObject(headlines))
                {
                    Debug.Log("RegisterHeadlineTextGameObject Unsuccessfull");
                }
            }
            else
                headlines.SetActive(false);

            string storeName = nativeAd.GetStore();
            if (storeName != null)
            {
                store.GetComponent<TextMeshProUGUI>().text = storeName;
                if (!nativeAd.RegisterStoreGameObject(store))
                {
                    Debug.Log("RegisterStoreGameObject Unsuccessfull");
                }
            }
            else
                store.SetActive(false);

            string bodyTextValue = nativeAd.GetBodyText();
            if (bodyTextValue != null)
            {
                bodyText.GetComponent<TextMeshProUGUI>().text = bodyTextValue;
                if (!nativeAd.RegisterBodyTextGameObject(bodyText))
                {
                    Debug.Log("RegisterBodyTextGameObject Unsuccessfull");
                }
            }
            else
                bodyText.SetActive(false);

            double starRatingValue = nativeAd.GetStarRating();
            if (starRating != null && starRatingValue >= 0) //change that ugly logic
            {
                coloredStarsRoot.GetComponent<Image>().fillAmount = (float)starRatingValue / 5;
            }
            else
            {
                starRating.SetActive(false);
                store.SetActive(false);
                //bodyText.SetActive(true);
            }

            if (nativeAd.GetImageTextures().Count > 0)
            {
                List<Texture2D> goList = nativeAd.GetImageTextures();
                bigImage.GetComponent<RawImage>().texture = goList[0];
                List<GameObject> list = new List<GameObject>();
                list.Add(bigImage.gameObject);
            }
            else
                bigImage.SetActive(false);

            string buttonTextString = nativeAd.GetCallToActionText();
            if (buttonTextString != null)
            {
                buttonText.GetComponent<TextMeshProUGUI>().text = buttonTextString;
                if (!nativeAd.RegisterCallToActionGameObject(buttonText))
                {
                    Debug.Log("RegisterCallToActionGameObject Unsuccessfull");
                }
            }
            else
            {
                buttonText.SetActive(false);
                buttonBack.SetActive(false);
            }
        }
    }

    public void RequestNativeAd()
    {
#if UNITY_ANDROID //|| UNITY_EDITOR
        string adUnitId = ADMOB_NATIVE_ID_ANDROID;
#elif UNITY_IPHONE
        string adUnitId = ADMOB_NATIVE_ID_IOS;
#else
        string adUnitId = "unexpected_platform";
#endif
        if (adLoader != null)
        {
            adLoader.OnUnifiedNativeAdLoaded -= HandleUnifiedNativeAdLoaded;
            adLoader.OnAdFailedToLoad -= HandleNativeAdFailedToLoad;
        }
        adLoader = new AdLoader.Builder(adUnitId)
            .ForUnifiedNativeAd()
            .Build();
        adLoader.OnUnifiedNativeAdLoaded += HandleUnifiedNativeAdLoaded;
        adLoader.OnAdFailedToLoad += HandleNativeAdFailedToLoad;
        adLoader.LoadAd(new AdRequest.Builder().Build());
    }

    private void HandleNativeAdFailedToLoad(object sender, AdFailedToLoadEventArgs args)
    {
        EnableDisableNativeAd(false);
        nativeAdPanelReady = false;
    }

    private void HandleUnifiedNativeAdLoaded(object sender, UnifiedNativeAdEventArgs args)
    {
        nativeAd = args.nativeAd;
        unifiedNativeAdLoaded = true;
        nativeAdPanelReady = true;
        EnableDisableNativeAd(true);
    }

    public void EnableDisableNativeAd(bool enabled)
    {
        if (nativeAdPanelReady)
            nativeAdPanel.SetActive(enabled);
    }
}